<h1>Programa python con ejemplo de un Http Server</h1>


Programa de ejemplo en python que despliega página web con un saludo en <br>
http://localhost:8000/
<br>
<h3>Instrucciones de ejecución</h3>
En la raíz del repo clonado, ejecutar:<br>
<em><b>python saludo.py</b></em>